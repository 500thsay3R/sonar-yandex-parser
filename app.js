/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const url = require('url');
const _ = require('lodash');
const cheerio = require('cheerio');
const request = require('request');
const querystring = require('querystring');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const countries = require('./lib/country-settings.json');

/** Class Representing Yandex Search request. */
class YandexSearch {
  /**
   * Create a search request.
   * @param {Object}    params                - The search parameters.
   * @param {!String}   params.query          - The search key.
   * @param {?String}   [params.country=null] - The country to get results for.
   * @param {?String}   [params.period=y]     - The search timeframe.
   * @param {?String}   [params.sort=rel]     - The sort order.
   * @param {String[]}  [params.inc=[]]       - The words every web-page must contain.
   * @param {String[]}  [params.exc=[]]       - The words web-pages should not contain.
   * @param {Number}    [params.limit=500]    - Defines # of resultant web-pages to retrieve.
   */
  constructor(params) {
    const { country = null, period = 'y', exc = [], inc = [], limit = 100 } = params;

    this.query = params.query;
    this.country = country;
    this.period = period;
    this.exclusions = exc;
    this.inclusions = inc;
    this.pages = Math.floor(limit / 10);
    this.results = new Set();
  }

  /**
   * Form up body of URL request.
   * @param {Number} [since=0] - paging parameter.
   * @returns {String} - the well-formed Yandex Search request URL
   *
   * @private
   */
  _formRequest(since = 0) {
    const requestOptions = {
      text: this.query,
      p: since,
    };

    // if the daily scan is being occurred impound 24h filtration
    if (this.period === 'd') {
      requestOptions.within = '77';
    }


    // make Yandex return country-specific results by using market parameter
    if (this.country && countries[this.country]) {
      requestOptions.rstr = countries[this.country].code;
    }

    // enhance search key by adding plus & minus-words
    if (this.exclusions.length) {
      requestOptions.text += ` -${this.exclusions.join(' -')}`;
    }
    if (this.inclusions.length) {
      requestOptions.text += ` +${this.inclusions.join(' +')}`;
    }

    return url.format({
      protocol: 'https:',
      host: 'yandex.com',
      pathname: '/search/',
      search: querystring.stringify(requestOptions),
    });
  }

  /**
   * Retrieves Yandex results.
   * @param {String} urlRequest   -  The well-formed URL to connect to.
   * @returns {Promise.<String>}  - The page contents.
   * @private
   */
  _performRequest(urlRequest) {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        url: url.format(urlRequest),
        timeout: 10000,
        rejectUnauthorized: false,  // prevents unauthorized connection from being dropped
      };

      request(requestOptions, (error, response) => {
        if (!error && response && response.statusCode === 200) {
          logger.debug(`@YANDEX :: Successfully retrieved data from ${urlRequest}`);

          // handle potential bans
          if (response && response.body.indexOf('class="main__center"') === -1) {
            logger.error('@YANDEX :: Yandex ban confirmed. Requests are temporarily forbidden!');

            reject(new Error('Yandex ban'));
          }

          resolve(response.body.toString());
        } else {
          logger.warn(`@YANDEX :: [${(response) ? response.statusCode : 'ERR'}] Failed to ` +
            `retrieve data from ${urlRequest} -> [${error || 'UNKNOWN ERROR'}].`);

          reject(new Error('Yandex connection failure'));
        }
      });
    });
  }

  /**
   * Parses data from each Yandex results page.
   *
   * @param {String} pageContents     -   HTML contents of Yandex results page.
   * @param {!Number} pageNumber      -   The number of page that is currently being processed.
   *
   * @returns {Promise.<Object[]>}    -   A bunch of resultant objects. Each of them includes:
   *          { url: {String}, title: {String}, description: {String}, index: {Number}}.
   * @private
   */
  _processPage(pageContents, pageNumber) {
    const $ = cheerio.load(pageContents);
    $('div.serp-adv-item').remove();

    // remove extra whitespaces & newline characters
    function prettyPrint(text) {
      return text.replace(/\n/g, ' ').replace(/\s+/g, ' ');
    }

    // analyse each and every resultant link element
    function analyze(elem, pageNum, position) {
      return new Promise((resolve) => {
        // distinguish links and their titles and textual description
        const text = $(elem).find('div.serp-item__text');
        const title = $(elem).find('h2.serp-item__title a.link');
        const link = $(elem).find('div.serp-url span a');

        const searchItem = {
          index: pageNumber * 10 + position,
          url: $(link).attr('href'),
          title: prettyPrint($(title).text()),
          description: prettyPrint($(text).text()),
        };

        // omit empty elements
        resolve(searchItem.url ? searchItem : null);
      });
    }

    // process all the result elements in parallel
    const resultElements = $('div.serp-item');
    const tasks = _.map(resultElements, (elem, index) => analyze(elem, pageNumber, ++index));

    // check if the next page of results does indeed exist
    const moreToCome = !!($('a.pager__item_kind_next').text());

    return Promise.all(tasks)
      .then(results => {
        return { results: _.compact(results), moreToCome };
      });
  }

  /**
   * Performs consecutive results extraction & processing
   * @param {!Number} currentPage   -   Current page of Yandex Search results
   * @param {!Number} since         -   Paging step.
   * @returns {Promise.<Object[]>}  -   Retrieved results.
   */
  find(currentPage = 0, since = 0) {
    return new Promise((resolve, reject) => {
      logger.debug(`@YANDEX:: PROCESSING PAGE #${currentPage + 1}.`);

      // perform extraction until the page limit is reached
      if (currentPage < this.pages) {
        this._performRequest(this._formRequest(since))
          .then(html => this._processPage(html, currentPage))
          .then(data => {
            logger.info(`@YANDEX :: ${data.results.length} links parsed from p.${currentPage}. ` +
              `More to come: ${data.moreToCome}`);

            _.map(data.results, dataElement => this.results.add(dataElement));

            // if there's no more pages to process, return results or
            // process the next page otherwise
            resolve(data.moreToCome ? this.find(++currentPage, ++since) : [...this.results]);
          })
          .catch(error => reject(error));
      } else {
        resolve([...this.results]);
      }
    });
  }
}

module.exports = YandexSearch;
